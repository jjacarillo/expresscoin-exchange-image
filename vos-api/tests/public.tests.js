var vos = require('../index'),
    vosclient = vos('4022e37630641a841862514ac92fa4e3872957f31b9c1a377ba66828e0e79ff2', '7387b55db7b26624b8251146a1ef78652233e860af1872472495d8e1dce64b0d');

var currency_pair = {
    order_currency: 'btc',
    payment_currency: 'usd'
};

var global = {
    passes: 0,
    fails: 0,
    _running: 0
};

function dump() {
    if(global._running === 0) {
        console.log('+--------------------------');
        console.log('| Report');
        console.log('| Passes: ' + global.passes);
        console.log('| Fails: ' + global.fails);
        console.log('+--------------------------');
    }
}

function testing(endpoint) {
    global._running++;
    return function(data) {
        console.log('----------------------------');
        console.log('Testing Endpoint: ' + endpoint);

        console.log('Request Success: ' + data.status);
        if(data.status) {
            global.passes++;
            if(data.status == 'success') {
                console.log(data);
            }
        }
        else {
            global.fails++;
        }

        global._running--;

        if(global._running == 0) {
            setTimeout(dump, 1000);
        }
    }
}

vosclient.Public.ticker(currency_pair, testing('/public/ticker'));
vosclient.Public.orderbook(currency_pair, testing('/public/orderbook'));
