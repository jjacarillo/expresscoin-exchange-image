var merge = require('merge');

var status = function(req, res) {
	res.send({
		status : "OK",
		version : "VOS-Exchange image v0.0.1",
		is_healthy : true
	});
};

function checkDCPrice(book, quantity, type, payment_currency, callback) {
	var total_quantity = 0;
	for (var i = 0; i < book.length; i++) {
		var current_price = book[i].price.value;
		var current_quantity = book[i].quantity.value;
		total_quantity = total_quantity + parseFloat(current_quantity, -1.);
		if (total_quantity > quantity) {
			callback({
				price : current_price,
				currency : payment_currency,
				type : type
			});
			return;
		}
	}
	callback({
		error : "order too large to fill"
	});
};

function checkFiatPrice(book, amount, type, payment_currency, callback) {
	var total_size = 0;
	for (var i = 0; i < book.length; i++) {
		current_price = book[i].price.value;
		quantity = book[i].quantity.value;
		var size = parseFloat(current_price, -1.) * parseFloat(quantity, -1.);
		total_size = total_size + size;
		if (total_size > amount) {
			callback({
				price : current_price,
				currency : payment_currency,
				type : type
			});
			return;
		}
	}
	callback({
		error : "order too large to fill"
	});
}

function orderbookPrice(options, vos, amount, type, fill_type, callback) {
	vos.Public.orderbook(options, function(err, r, book) {
		if (err) {
			console.log(err);
		}
		var total_size = 0.;
		if (book.status === 'success') {
			if (type === 'buy') {
				book = book.data.asks;
			} else {
				book = book.data.bids;
			}
			if (fill_type === 'fiat')
				checkFiatPrice(book, amount, type, options.payment_currency, callback);
			else
				checkDCPrice(book, amount, type, options.payment_currency, callback);
		} else {
			console.log('VOS ERROR');
			console.log(book);
			callback({
				error : book.message
			});
		}
	});
}

var price = function(req, res) {
	var currency = req.body.currency;
	var amount = req.body.amount;
	var type = req.body.type;
	var key = req.body.key;
	var secret = req.body.secret;
	var vos = require('./vos-api')(key, secret);
	var options = {
		order_currency : 'BTC',
		payment_currency : currency,
		count : 100
	};
	orderbookPrice(options, vos, amount, type, 'fiat', function(result) {
		res.send(result);
	});
};

var priceDC = function(req, res) {
	console.log(req.body);
	var currency = req.body.currency;
	var quantity = req.body.quantity;
	var type = req.body.type;
	var key = req.body.key;
	var secret = req.body.secret;
	var vos = require('./vos-api')(key, secret);
	var options = {
		order_currency : 'BTC',
		payment_currency : currency,
		count : 100
	};
	orderbookPrice(options, vos, quantity, type, 'dc', function(result) {
		res.send(result);
	});
};

var balance = function(req, res) {
	console.log(req.body);
	var currency = req.body.currency;
	var key = req.body.key;
	var secret = req.body.secret;
	var vos = require('./vos-api')(key, secret);
	var options = {
		currency : currency
	};
	vos.Info.balance(options, function(err, r, data) {
		if (err) {
			console.log(err);
			res.send({
				error : err
			});
		}
		else if (data.status === 'success') {
			console.log(data);
			res.send({
				balance : data.data.value,
				currency : currency
			});
		} else {
			console.log('VOS ERROR');
			console.log(data);
			res.send({
				error : data.message
			});
		}
	});
};

var withdraw = function(req, res) {
	var key = req.body.key;
	var secret = req.body.secret;
	var currency = req.body.currency;
	var quantity = req.body.quantity;
	var address = req.body.address;
	var vos = require('./vos-api')(key, secret);
	var options = {
		currency : currency,
		quantity : quantity,
		address : address
	};
	vos.Withdraw.transfer(options, function(err, r, data) {
		if (err) {
			res.send({
				error : err
			});
		}
		else if (data.status === 'success') {
			console.log(data);
			res.send({
				txid : data.data.txid
			});
		} else {
			console.log('VOS ERROR');
			console.log(data);
			res.send({
				error : data.message
			});
		}
	});
};

var placeOrder = function(req, res) {
	var key = req.body.key;
	var secret = req.body.secret;
	var vos = require('./vos-api')(key, secret);
	var buffer = parseFloat(req.body.buffer);
	var type = req.body.type;
	var digital_curreny = req.body.digital_currency;
	var fiat_currency = req.body.fiat_currency;
	var wfid = req.body.wfid;
	var quantity;
	if (type === 'buy') {// AMOUNT DC TO BUY
		quantity = req.body.amount_dc;
		fill_type = 'dc';
	} else if (type === 'sell') {//AMOUNT FIAT to SELL
		quantity = req.body.amount_fiat;
		fill_type = 'fiat';
	} else {
		//fuck you vignesh
	}
	var options = {
		order_currency : digital_curreny,
		payment_currency : fiat_currency,
		round : 2,
		count : 100
	};

	orderbookPrice(options, vos, quantity, type, fill_type, function(result) {
		if (result.error) {
			res.send({
				error : result.error,
				wfid : wfid
			});
		} else {
			console.log('Initial price');
			price = result.price;
			console.log(price);
			var trade_options;
			if (type === 'buy') {
				price = price * (1 + buffer );
				console.log('Buffered price');
				console.log(price);
				var amount = quantity;
				//Satoshis
				bit_obj = toCurrencyObjDigital(8, amount);
				price_obj = toCurrencyObjFiat(2, price);
				trade_options = {
					type : 'bid',
					order_currency : digital_curreny,
					payment_currency : fiat_currency,
					units : bit_obj,
					price : price_obj
				};
				console.log(trade_options);
			} else if (type === 'sell') {
				price = price * (1 - buffer );
				console.log('Buffered price');
				console.log(price);
				var amount = quantity / price;
				//Dollars
				bit_obj = toCurrencyObjDigital(8, amount);
				price_obj = toCurrencyObjFiat(2, price);
				trade_options = {
					type : 'ask',
					order_currency : digital_curreny,
					payment_currency : fiat_currency,
					units : bit_obj,
					price : price_obj
				};
				console.log(trade_options);
			} else {
				//impossible
			}
			console.log('trading');
			vos.Trade.place(trade_options, function(err, r, result) {
				if (err) {
					res.send({
						error : err,
						wfid : wfid
					});
				} else if (result.status === "success") {
					console.log(result);
					res.send({
						order_id : result.data.order_id,
						wfid : wfid,
						price_placed : price
					});
				} else {
					res.send({
						error : result.message,
						wfid : wfid
					});
				}
			});

		}
	});
};

var getOrders = function(req, res) {
	var key = req.body.key;
	var secret = req.body.secret;
	var number = req.body.number;
	var vos = require('./vos-api')(key, secret);
	var options = {
		count : number
	};
	vos.Info.orders(options, function(err, r, data) {
		if (err) {
			res.send({
				error : err
			});
		} else if (data.status === "success") {
			list = data.data;
			var closed = [];
			var open = [];
			var unknown = [];
			for (var i = 0; i < list.length; i++) {
				var current_order = list[i];
				if (current_order.status === 'placed') {
					open.push({
						order_id : current_order.order_id
					});
				} else if (current_order.status === 'filled') {
					closed.push({
						order_id : current_order.order_id
					});
				} else {
					unknown.push({
						order_id : current_order.order_id
					});
				}
			}
			var l = {
				closed : closed,
				open : open,
				unknown : unknown
			};
			res.send(l);
		} else {
			res.send({
				error : data.message
			});
		}
	});
};

var getOrderDetails = function(req, res) {
	var key = req.body.key;
	var secret = req.body.secret;
	var order_id = req.body.order_id;
	var vos = require('./vos-api')(key, secret);
	var options = {
		order_id : order_id
	};
	console.log(order_id);
	vos.Info.order_detail(options, function(err, r, data) {
		if (err) {
			res.send({
				error : err
			});
		} else if (data.status === "success") {
			console.log(data);
			
			orders = data.data;
			var l;
			if (orders.length === 0) {
				console.log('empty order');
				l = {
					order_id : order_id,
					status : 'open'
				};
			} else {
				var type;
				var status;
				var order;
				total_digital_currency = 0;
				total_fiat_currency = 0.;
				total_fee= 0.;
				
				for (var i = 0; i < orders.length; i++) {
					var order = orders[i];
					type = (order.type == 'bought' ? "buy" : "sell");
					status = 'closed';
					order = orders[i];
					total_digital_currency += parseFloat(order.units_traded.value,0);
					total_fiat_currency += parseFloat(order.total.value,0);
					console.log(order.fee);
					total_fee += parseFloat(order.fee.value,0);;
				}
				l = {
					order_id : order_id,
					average_price : order.price.value,
					type : type,
					status : status,
					total_digital_currency : total_digital_currency.toString(),
					total_fiat_currency: total_fiat_currency,
					fee: { 
						fiat_currency:total_fee
					}
				};
			}

			res.send(l);
		} else {
			res.send({
				error : data.message
			});
		}
	});
};

var toCurrencyObjFiat = function(precision, value) {
	try{
		value = parseFloat(value);
	}catch(err){
		value = 0.;
	}
	value_int = value * (Math.pow(10, precision));
	value_int = Math.floor(value_int);
	return {
		precision : precision,
		value : value.toString(),
		value_int : value_int
	};
};

var toCurrencyObjDigital = function(precision, value) {
	try{
		value = parseFloat(value);
	}catch(err){
		value = 0.;
	}
	value_int = value.toFixed(precision) * (Math.pow(10, precision));
	value_int = Math.floor(value_int);
	return {
		precision : precision,
		value : value.toFixed(precision),
		value_int : value_int
	};
};

exports.status = status;
exports.price = price;
exports.priceDC = priceDC;
exports.balance = balance;
exports.placeOrder = placeOrder;
exports.getOrders = getOrders;
exports.getOrderDetails = getOrderDetails;
exports.withdraw = withdraw;
