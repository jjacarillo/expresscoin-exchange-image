'use strict';

var should = require('should');
var assert = require('assert');
var request = require('supertest');

var app = require('./../server.js');
var port = 9050;

describe('VOS-API', function() {
	var url = "http://localhost:" + port.toString();
	var key = '';
	var secret = '';
	var order1 = '';
	var order2 = '';
	var order3 = '';
	it('return a buy price', function(done) {
		console.log('Getting BUY Price\n\n');
		var query = {
			currency : 'CAD',
			amount : 500,
			type : 'buy',
			key : key,
			secret : secret
		};
		request(url).post('/v1/price').send(query)
		// end handles the response
		.end(function(err, res) {
			if (err) {
				throw err;
			}
			console.log(res.body);
			(res.body.error === undefined).should.equal(true);
			res.body.price.should.not.equal(undefined);
			done();
		});
	});
	it('return a sell price', function(done) {
		console.log('Getting SELL Price\n\n');
		var query = {
			currency : 'CAD',
			amount : 500,
			type : 'sell',
			key : key,
			secret : secret
		};
		request(url).post('/v1/price').send(query)
		// end handles the response
		.end(function(err, res) {
			if (err) {
				throw err;
			}
			console.log(res.body);
			(res.body.error === undefined).should.equal(true);
			res.body.price.should.not.equal(undefined);
			done();
		});
	});
	it('return a DC buy price', function(done) {
		console.log('Getting DC buy Price\n\n');
		var query = {
			currency : 'CAD',
			quantity : 20,
			type : 'buy',
			key : key,
			secret : secret
		};
		request(url).post('/v1/priceDC').send(query)
		// end handles the response
		.end(function(err, res) {
			if (err) {
				throw err;
			}
			console.log(res.body);
			(res.body.error === undefined).should.equal(true);
			res.body.price.should.not.equal(undefined);
			done();
		});
	});
	it('return a balance', function(done) {
		console.log('Getting Balance\n\n');
		var query = {
			currency : 'CAD',
			key : key,
			secret : secret
		};
		request(url).post('/v1/balance').send(query)
		// end handles the response
		.end(function(err, res) {
			if (err) {
				throw err;
			}
			console.log(res.body);
			(res.body.error === undefined).should.equal(true);
			res.body.balance.should.not.equal(undefined);
			done();
		});
	});
	it('return a balance', function(done) {
		console.log('Getting Balance\n\n');
		var query = {
			currency : 'BTC',
			key : key,
			secret : secret
		};
		request(url).post('/v1/balance').send(query)
		// end handles the response
		.end(function(err, res) {
			if (err) {
				throw err;
			}
			console.log(res.body);
			(res.body.error === undefined).should.equal(true);
			res.body.balance.should.not.equal(undefined);
			done();
		});
	});
	it('place a buy order', function(done) {
		console.log('BUY Order\n\n');
		
		var query = {
			key: key,
			secret: secret,
			buffer: '0.01',
	 		type: 'buy',
	 		digital_currency: 'BTC',
	 		fiat_currency: 'CAD',
	 		//amount_double: 10.0,
	 		amount_dc: '0.01',//Double (SELL: amount of DC to sell, in SATOSHI)
			wfid: 1
		};
		request(url).post('/v1/placeOrder').send(query)
		// end handles the response
		.end(function(err, res) {
			if (err) {
				throw err;
			}
			console.log(res.body);
			(res.body.error === undefined).should.equal(true);
			res.body.order_id.should.not.equal(undefined);
			order1 = res.body.order_id;
			done();
		});
	});
	it('place a sell order', function(done) {
		console.log('SELL Order\n\n');
		
		var query = {
			key: key,
			secret: secret,
			buffer: '0.01',
	 		type: 'sell',
	 		digital_currency: 'BTC',
	 		fiat_currency: 'CAD',
	 		amount_fiat: '10.0',
	 		//amount_int: 0.01,//Double (SELL: amount of DC to sell, in SATOSHI)
			wfid: 1
		};
		request(url).post('/v1/placeOrder').send(query)
		// end handles the response
		.end(function(err, res) {
			if (err) {
				throw err;
			}
			(res.body.error === undefined).should.equal(true);
			res.body.order_id.should.not.equal(undefined);
			order2 = res.body.order_id;
			done();
		});
	});
	it('should get all orders', function(done) {
		console.log('All Orders\n\n');
		
		var query = {
			key: key,
			secret: secret,
			number: 1000
		};
		request(url).post('/v1/getOrders').send(query)
		.end(function(err, res) {
			if (err) {
				throw err;
			}
			(res.body.error === undefined).should.equal(true);
			res.body.closed.should.not.equal(undefined);
			order3 = res.body.closed[1].order_id;
			done();
		});
	});
	it('should get specific order', function(done) {
		console.log('Specific Order\n\n');
		
		var query = {
			key: key,
			secret: secret,
			order_id : order3
		};
		request(url).post('/v1/getOrderDetails').send(query)
		.end(function(err, res) {
			if (err) {
				throw err;
			}
			console.log(res.body);
			(res.body.error === undefined).should.equal(true);
			res.body.order_id.should.not.equal(undefined);
			done();
		});
	});
	it('should withdraw btc to the account', function(done) {
		console.log('Withdrawal\n\n');
		
		var query = {
			key: key,
			secret: secret,
			currency : 'BTC',
			quantity : '0.001',
			address : '1LH9hsBW5Bt5D3GuQJ2kFSFzD1yxYyyRN1'
			
		};
		request(url).post('/v1/withdraw').send(query)
		.end(function(err, res) {
			if (err) {
				throw err;
			}
			console.log(res.body);
			(res.body.error === undefined).should.equal(true);
			res.body.txid.should.not.equal(undefined);
			done();
		});
	});
});

