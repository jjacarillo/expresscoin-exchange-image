## Expresscoin exchange service provider Docker image


	
	get /health
	- is callback_url set

	post /price
	- key
	- secret
	- price : double
	- currency: String
	- type: buy/sell (buy - buy bitcoins)

	return:
		error (error message if I have it)

		price (double)

	get Balance
	- key
	- secret
	- currency : String (CAD)

	return:
		error (error message if I have it)

		balance : double
		currency : str

	POST /placeOrder
	- key
	- secret
	- buffer: Double (percentage to increase bid by to ensure filling. Example: for 5%, send me 0.05)
	- type: buy/sell (str) // We are either buying or selling DIGITAL CURRENCY!
	- digital_curreny: (str)
	- fiat_currency: (str)
	- amount_fiat: Double (if SELL: amount of FIAT to SELL (What we are going to dispense))
	- amount_dc: Double (BUY: amount of DC to BUY as DECIMAL)
	- wfid: int
		return:
			wfid: int
			error (error message if I have it)

			wfid: int
			order_id: int
			price_placed: Double (FIAT)

	POST /getOrders (will initate polling of orders
	- key
	- secret
	- number: int (total orders to poll)

		return:
			error (error message if I have it)

			closed:	(JSON LIST)
				order_id: String
			
			open: (JSON LIST)
				order_id: String

			unknown: (JSON LIST
				order_id: String


	GET /orderDetails
	- key
	- secret
	- order_id: String

		return:
			error (error message if I have it)
			order_id: String
			status: open (NOTHING ELSE WILL BE RETURNED
			
			status: closed (now you get info) //DO NOT TRUST THIS, COULD BE PARTIALLY EXECUTED!
			average_price: String
			type: String (buy, sell)
			status: String (open, closed, error)
			total_digital_currency: Int
			total_fiat_currency:double
			fee: {digital_currency:int,
				fiat_currency:double}
				
	POST /withdraw
	- key
	- secret
	- currency : 'BTC'
	- quantity : String (up to 8 decimal places)
	- address : String (address)

		return:
			error (error message if I have it)
			
			txid: String of txid